#!/bin/bash

sudo apt-get -y remove docker;
sudo apt-get -y remove docker-engine;
sudo apt-get -y remove docker.io;

curl -fsSL get.docker.com -o get-docker.sh

sudo sh get-docker.sh

sudo usermod -aG docker $USER

rm get-docker.sh


