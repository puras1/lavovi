#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

BASE_PATH=$SCRIPTPATH/..

export SHARED_HOST_DIR="$BASE_PATH/shared_dir"

echo "Path to shared dir: ${SHARED_HOST_DIR}"

$BASE_PATH/docker/generic/run.sh -b $BASE_PATH/autoware.ai/ -c off -r kinetic

