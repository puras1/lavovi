#!/bin/bash

mkdir -p autoware.ai/src

cd autoware.ai

wget -O autoware.ai.repos "https://gitlab.com/autowarefoundation/autoware.ai/autoware/raw/1.12.0/autoware.ai.repos?inline=false"

vcs import src < autoware.ai.repos

rosdep update
rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO
