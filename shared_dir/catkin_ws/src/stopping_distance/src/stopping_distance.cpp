#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/PointCloud2.h"
#include "geometry_msgs/Vector3Stamped.h"
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <time.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <pcl/filters/passthrough.h>
#include <visualization_msgs/Marker.h>
#include <pcl/point_types.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>
#include <math.h>

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

void chatterCallbackLidar(const sensor_msgs::PointCloud2::ConstPtr& msg);
void chatterCallbackSpeed(const geometry_msgs::Vector3Stamped::ConstPtr& msg);
void frameTransform();
void getParams(ros::NodeHandle& n);
pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFilter(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloudIn);
double calculateFreeArea();
void markerParams();
void textMarkerParams();
void stoppingMarkerParams();
void updateMarker(double min);
void updateStoppingMarker(double stoppingDistance);


typedef struct ourParams{
	double lane_width;
	double max_mon_distance;
	double max_mon_height;
	double min_mon_height;
	double max_vehicle_deceleration;
	std::string lidar_data;
	std::string velocity;
	std::string stopping_dist_marker;
	std::string coordinate_frame;
	
}params;

//global variable
tf::StampedTransform transform;
params param;
int skipCount = 0;
pcl::PointCloud<pcl::PointXYZ>::Ptr finalCloud;
visualization_msgs::Marker marker;
visualization_msgs::Marker stoppingMarker;
visualization_msgs::Marker textMarker;
double min_history=14.0;


int main(int argc, char **argv)
{

	ros::init(argc, argv, "stopping_distance");
	ros::NodeHandle n;
	
	
	getParams(n);
	markerParams();
	textMarkerParams();
	stoppingMarkerParams();
	
	
	frameTransform();
	
	ros::Subscriber sub_lidar = n.subscribe("/points_raw", 10, chatterCallbackLidar);
	ros::Subscriber sub_speed = n.subscribe("/estimated_vel", 10, chatterCallbackSpeed);
	ros::Publisher pub_stop_dist = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);
	ros::Rate loop_rate(10);

	while(ros::ok())
	{
		ros::spinOnce();
		pub_stop_dist.publish(marker);
		pub_stop_dist.publish(textMarker);
		pub_stop_dist.publish(stoppingMarker);
	
	
	}
	return 0;
}

void chatterCallbackLidar(const sensor_msgs::PointCloud2::ConstPtr& msg)
{
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudTransformed(new pcl::PointCloud<pcl::PointXYZ>);
	
	pcl::fromROSMsg (*msg, *cloud); 
	
	pcl_ros::transformPointCloud(*cloud, *cloudTransformed, transform);
	
	finalCloud = cloudFilter(cloudTransformed);
	
	double min = 14.0;
	min = calculateFreeArea();
	
	updateMarker(min);

}

void chatterCallbackSpeed(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
	double velocity = msg->vector.x;
	
	double stoppingDistanceText = (pow(velocity, 2.0))/(-2*param.max_vehicle_deceleration);
	
	double stoppingDistance = (pow(velocity, 2.0))/(-2*param.max_vehicle_deceleration) + 4.0; //4.0 is added to represent car lenght 

	textMarker.text = "Car Speed: " + std::to_string(velocity) + "[m/s] Stopping Distance: " + std::to_string(stoppingDistanceText) + "[m]";
	
	updateStoppingMarker(stoppingDistance);
	
}

void frameTransform()
{
	ros::Time timeNow;
	tf::TransformListener tfListener;
	try{

    tfListener.waitForTransform("/base_link", "/velodyne",
                              timeNow, ros::Duration(10.0));
	tfListener.lookupTransform("/base_link", "/velodyne", timeNow, transform);
	}
	catch(tf::TransformException ex)
	{
		ROS_ERROR("%s", ex.what());
		ros::Duration(1.0).sleep();
	}
}


void getParams(ros::NodeHandle& n)
{
	n.getParam("lane_width", param.lane_width);
	n.getParam("max_mon_distance", param.max_mon_distance);
	n.getParam("max_mon_height", param.max_mon_height);
	n.getParam("min_mon_height", param.min_mon_height);
	n.getParam("max_vehicle_deceleration", param.max_vehicle_deceleration);
	
	n.getParam("/points_raw", param.lidar_data); 
	n.getParam("/estimated_vel", param.velocity); 
	n.getParam("/stopping_distance", param.stopping_dist_marker);
	n.getParam("/base_link", param.coordinate_frame);
}

pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFilter(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloudIn)
{

	pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFiltered(new pcl::PointCloud<pcl::PointXYZ>);
	
	pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond (new pcl::ConditionAnd<pcl::PointXYZ> ());
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, 4.0)));
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, param.max_mon_distance)));
	
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, -(param.lane_width/2))));
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT,param.lane_width/2)));
	
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, param.min_mon_height)));
	range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::LT, param.max_mon_height)));
	// build the filter
	pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
	condrem.setCondition (range_cond);
	condrem.setInputCloud (cloudIn);
	// apply filter
	condrem.filter (*cloudFiltered);
		
	return cloudFiltered;

}

double calculateFreeArea()
{
	double min = param.max_mon_distance;
	double z = 0.0;
	double y = 0.0;
	double temp = 0.0;
	
	for(const auto& point : *finalCloud)
	{	
		
		if(point.x < min)
		{
			min = point.x;
			z = point.z;
			y = point.y;	
			
		}
	}
	
	return min;
}

void markerParams()
{
	marker.header.frame_id = "/base_link"; 
	marker.header.stamp = ros::Time::now();
		
	marker.ns = "free_area_shape";
	marker.id = 0;
		
	marker.type = visualization_msgs::Marker::CUBE;	
	marker.action = visualization_msgs::Marker::ADD; 
	
	 marker.pose.position.x = 0;
	 marker.pose.position.y = 0;
     marker.pose.position.z = 0;
     marker.pose.orientation.x = 0.0;
     marker.pose.orientation.y = 0.0;
     marker.pose.orientation.z = 0.0;
     marker.pose.orientation.w = 1.0;
  
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
     marker.scale.x = 14.0;
     marker.scale.y = 2.0;
     marker.scale.z = 0.1;
  
    // Set the color -- be sure to set alpha to something non-zero!
     marker.color.r = 0.0f;
     marker.color.g = 0.0f;
     marker.color.b = 1.0f;
     marker.color.a = 1.0;
  
     marker.lifetime = ros::Duration();

	return;
}

void stoppingMarkerParams()
{
	stoppingMarker.header.frame_id = "/base_link"; 
	stoppingMarker.header.stamp = ros::Time::now();
		
	stoppingMarker.ns = "stopping_marker";
	stoppingMarker.id = 2;
		
	stoppingMarker.type = visualization_msgs::Marker::CUBE;	
	stoppingMarker.action = visualization_msgs::Marker::ADD;
	
	 stoppingMarker.pose.position.x = 0;
	 stoppingMarker.pose.position.y = 0;
     stoppingMarker.pose.position.z = 0;
     stoppingMarker.pose.orientation.x = 0.0;
     stoppingMarker.pose.orientation.y = 0.0;
     stoppingMarker.pose.orientation.z = 0.0;
     stoppingMarker.pose.orientation.w = 1.0;
  
    // Set the scale of the marker -- 1x1x1 here means 1m on a side
     stoppingMarker.scale.x = 0.0;
     stoppingMarker.scale.y = 2.0;
     stoppingMarker.scale.z = 0.2;
  
    // Set the color -- be sure to set alpha to something non-zero!
     stoppingMarker.color.r = 0.0f;
     stoppingMarker.color.g = 1.0f;
     stoppingMarker.color.b = 0.0f;
     stoppingMarker.color.a = 1.0;
  
     stoppingMarker.lifetime = ros::Duration();

	return;
}

void textMarkerParams()
{
textMarker.header.frame_id = "/base_link";
textMarker.header.stamp = ros::Time::now();
textMarker.ns = "text_shapes";
textMarker.id = 1;
textMarker.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
textMarker.action = visualization_msgs::Marker::ADD;

textMarker.pose.position.x = 1.0;
textMarker.pose.position.y = 2.5;
textMarker.pose.position.z = 1.0;
textMarker.pose.orientation.x = 0.0;
textMarker.pose.orientation.y = 0.0;
textMarker.pose.orientation.z = 0.0;
textMarker.pose.orientation.w = 1.0;

textMarker.text = "Car Speed:";

textMarker.scale.x = 0.3;
textMarker.scale.y = 0.3;
textMarker.scale.z = 0.1;

textMarker.color.r = 0.0f;
textMarker.color.g = 1.0f;
textMarker.color.b = 0.0f;
textMarker.color.a = 1.0;
}

void updateStoppingMarker(double stoppingDistance)
{
	
	stoppingMarker.pose.position.x = 4+(stoppingDistance-4)/2;
	stoppingMarker.pose.position.y = 0;
    stoppingMarker.pose.position.z = 0;
    stoppingMarker.scale.x = stoppingDistance-4.0;
    
    if(stoppingMarker.scale.x > marker.scale.x)
    {
     stoppingMarker.color.r = 1.0f;
     stoppingMarker.color.g = 0.0f;
	}
	else
	{
	 stoppingMarker.color.r = 0.0f;
     stoppingMarker.color.g = 1.0f;
	}
    
    
    
	return;
}

void updateMarker(double min) 
{
	marker.pose.position.x = 4+(min-4)/2;
	marker.pose.position.y = 0;
    marker.pose.position.z = 0;
    marker.scale.x = min-4.0;
    
	return;
}
